tcp-proj
========

http://people.cs.clemson.edu/~westall/853/notes/skbuff.pdf

https://web.archive.org/web/20060114000005/http://jungla.dit.upm.es/~jmseyas/linux/kernel/hackers-docs.html

https://linuxwarrior.wordpress.com/2008/12/02/add-a-new-protocol-to-linux-kernel/

http://lxr.free-electrons.com/source/include/net/tcp.h

These instructions come from a StackOverflow question: http://stackoverflow.com/questions/13760017/addition-of-a-new-network-protocol-in-the-linux-kernel

You will need to register your protocol with the sockets API to handle communication from userspace to your protocol.

Have a look at the bluetooth/RFCOM socket implementation for relevant code samples.

```c
static const struct proto_ops rfcomm_sock_ops = {
     .family         = PF_BLUETOOTH,
     .owner          = THIS_MODULE,
     .bind           = rfcomm_sock_bind,
     .connect        = rfcomm_sock_connect,
     .listen         = rfcomm_sock_listen,
     .
     .
     .
     .accept         = rfcomm_sock_accept,

};

static const struct net_proto_family rfcomm_sock_family_ops = {
     .family         = PF_BLUETOOTH,
     .owner          = THIS_MODULE,
     .create         = rfcomm_sock_create
};
```

To register a protocol you will have to fill the proto_ops structure. This structure follows the object oriented pattern observed elsewhere inside the kernel. This structure defines an interface to follow for developers implementing their own socket interface.

1. Implement the functions the interface defines such as bind, connect, listen, and assign the function pointer to the structure entry. Define ioctl's for functionality not covered by the operations interface.

You end up with a structure that later you embed at the socket struct we return from the create function.

Struct net_proto_family defines a new protocol family. This structure includes the create function where your function implementation should populate a socket struct filled with the proto_ops struct.

2. After that register the family with sock_register, and if everything is ok you should be able to create a proper socket from userspace.

Internally the protocol should probably use skbuffs[1],[2],[3](pdf). to communicate with the networking devices.

skbuffs are the universal way of handling network packets in the linux kernel. The packets are received by the network card, put into some skbuffs and then passed to the network stack, which uses the skbuff all the time.

This is the basic data structure and io path to implement a networking protocol inside the linux kernel.
