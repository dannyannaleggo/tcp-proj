#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

void handle_tcp_client(int clientfd);

int main(int argc, char **argv)
{
	int socketfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (-1 == socketfd) {
		perror("??");
		exit(-1);
	}

	struct sockaddr_in my_socket, client_socket;
	memset(&my_socket, 0, sizeof(struct sockaddr_in));
	// initialize myself
	my_socket.sin_family = AF_INET;
	my_socket.sin_addr.s_addr = htonl(INADDR_ANY);
	my_socket.sin_port = 8080;

	// bind to port
	if (0 != bind(socketfd, (const struct sockaddr *)&my_socket,
		      sizeof(struct sockaddr_in))) {
		perror("??");
		exit(-1);
	}

	// listen for connections
	if (0 != listen(socketfd, 1)) {
		perror("??");
		exit(-1);
	}

	while (true) {
		unsigned addrlen = sizeof(struct sockaddr_in);
		int client_fd = accept(
		    socketfd, (struct sockaddr *)&client_socket, &addrlen);
		if (client_fd < 0) {
			perror("??");
			exit(-1);
		}
		printf("Handling a new client!!!!!!! at %s\n",
		       inet_ntoa(client_socket.sin_addr));
		handle_tcp_client(client_fd);
	}

	if (0 != close(socketfd)) {
		perror("??");
		exit(-1);
	}
}

void handle_tcp_client(int clientfd)
{
	const size_t bufsize = 1024;
	char buffer[bufsize];
	int received_bytes = -1;
	int sent_bytes;
	while (true) {
		received_bytes = recv(clientfd, buffer, bufsize, 0);
		if (0 == received_bytes) {
			break;
		}
		if (-1 == received_bytes) {
			perror("??");
			exit(-1);
		}
		buffer[received_bytes] = '\0';
		printf("%s\n", buffer);
		sent_bytes = send(clientfd, buffer, received_bytes, 0);
		if (sent_bytes != received_bytes) {
			perror("??");
			exit(-1);
		}
	}
}
