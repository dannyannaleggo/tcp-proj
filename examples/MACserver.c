#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include "../module/MACheader.h"

void handle_client(int clientfd);

int main()
{
	printf("RUNNING ON PROTOCOL: %d\n", IPPROTO_MAC);
	int socketfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_MAC);
	if (-1 == socketfd) {
		perror("??a");
		exit(-1);
	}

	struct sockaddr_in my_socket;
	memset(&my_socket, 0, sizeof(struct sockaddr_in));
	// initialize myself
	my_socket.sin_family = AF_INET;
	my_socket.sin_addr.s_addr = htonl(INADDR_ANY);
	my_socket.sin_port = htons(8080);

	int rc = 0;

	/* test setting security value to valid value */
	int sec_code = 1;
	rc +=
	    setsockopt(socketfd, SOL_MAC, MAC_DOM, &sec_code, sizeof(sec_code));
	rc +=
	    setsockopt(socketfd, SOL_MAC, MAC_LEV, &sec_code, sizeof(sec_code));
	rc +=
	    setsockopt(socketfd, SOL_MAC, MAC_CAT, &sec_code, sizeof(sec_code));

	if (rc) {
		perror(":C");
		exit(-1);
	}

	int sec_code_check;
	socklen_t size_of_sock = sizeof(int);
	rc = getsockopt(socketfd, SOL_MAC, MAC_DOM, &sec_code_check,
			&size_of_sock);
	if (rc) {
		perror(":CCCC");
		exit(-1);
	}

        printf("check %d: code %d\n", sec_code_check, sec_code);
	if (sec_code_check != sec_code) {
		fprintf(stderr, "SADNESS: GETSOCKOPT != SETSOCKOPT\n");
		exit(-1);
	}

	/* test setting to invalid value */
	int unused = 0;
	/* MAC_CHANGE_SEC + 1 should fail here */
	rc =
	    setsockopt(socketfd, SOL_MAC, MAC_DOM + 3, &unused, sizeof(unused));
	if (rc) {
		printf("cool, stuff works\n");
		errno = 0;
	} else {
		fprintf(stderr, "INVALID OPTNAME WORKED SOMEHOW????\n");
		exit(-1);
	}

	// bind to port
	if (0 != bind(socketfd, (const struct sockaddr *)&my_socket,
		      sizeof(struct sockaddr_in))) {
		perror("?b");
		exit(-1);
	}

	handle_client(socketfd);

	if (0 != close(socketfd)) {
		perror("????");
		exit(-1);
	}
}

void handle_client(int socketfd)
{
	const size_t bufsize = 1024;
	char buffer[bufsize];
	int received_bytes = -1;
	/* int sent_bytes; */
	struct sockaddr_in client_addr;
	socklen_t size = sizeof(client_addr);
	while (true) {
		received_bytes =
		    recvfrom(socketfd, buffer, bufsize, 0,
			     (struct sockaddr *)&client_addr, &size);
		if (0 == received_bytes) {
			break;
		}
		if (-1 == received_bytes) { /* security mismatch */
			perror("?????");
			/* exit(-1); */
		} else {
			buffer[received_bytes] = '\0';
			printf("%s\n", buffer);
		}
		/* sent_bytes = sendto(socketfd, buffer, received_bytes, 0, */
		/* 		    (struct sockaddr *)&client_addr, size); */
		/* if (sent_bytes != received_bytes) { */
		/* 	perror("?????"); */
		/* 	exit(-1); */
		/* } */
	}
}
