#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

const unsigned int BUFFSIZE = 1024;

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "gimme something to say as an argument!\n");
		exit(-1);
	}

	char buffer[BUFFSIZE];

	int s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (!s) {
		printf("error: socket did not open successfully\n");
		perror("?awegaweg");
		exit(-1);
	}

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr("10.67.7.169");
	addr.sin_port = 8080;

	if (connect(s, (const struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("owaiegawieng");
		exit(-1);
	}

	// send the word to the server
	unsigned int echolen = strlen(argv[1]);
	if (send(s, argv[1], echolen, 0) != echolen) {
		perror("mismatch in number of sent bytes");
		exit(-1);
	}

	// receive the word back from the server
	int received = 0;
	fprintf(stdout, "Received: ");
	while (received < echolen) {
		int bytes = 0;
		if ((bytes = recv(s, buffer, BUFFSIZE - 1, 0)) < 1) {
			perror("faild to receive btyes from server");
			exit(-1);
		}
		received += bytes;
		buffer[bytes] = '\0';
		fprintf(stdout, "%s\n", buffer);
	}

	fprintf(stdout, "\n");
	close(s);
	exit(0);
}
