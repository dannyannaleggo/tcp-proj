#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

const unsigned int BUFFSIZE = 1024;

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "gimme something to say as an argument!\n");
		exit(-1);
	}

	char buffer[BUFFSIZE];

	int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (!s) {
		printf("error: socket did not open successfully\n");
		perror("?awegaweg");
		exit(-1);
	}

	struct sockaddr_in addr;
	memset((char *)&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr("10.67.7.169");
	addr.sin_port = htons(8080);
	socklen_t addrLen = sizeof(addr);
	/*
	    struct sockaddr_in myAddr;
	    memset((char *)&myAddr, 0, sizeof(myAddr));
	    myAddr.sin_family = AF_INET;
	    myAddr.sin_addr.s_addr = inet_addr("10.67.214.94");
	    myAddr.sin_port = htons(8080);
	    socklen_t myAddrLen = sizeof(myAddr);
	*/
	// send the word to the server
	unsigned int echolen = strlen(argv[1]);
	printf("argv[1]: %s\n", argv[1]);
	printf("strlen(argv[1]): %i\n", echolen);

	if (sendto(s, argv[1], echolen, 0, (const struct sockaddr *)&addr,
		   addrLen) < 0) {
		perror("sendto failed");
		exit(-1);
	}
	/*
		// receive the word back from the server
		int received = 0;
		fprintf(stdout, "Received: ");
		while (received < echolen) {
			int bytes = 0;
			if ((bytes = recv(s, buffer, BUFFSIZE - 1, 0)) < 1) {
				perror("failed to receive btyes from server");
				exit(-1);
			}
			received += bytes;
			buffer[bytes] = '\0';
			fprintf(stdout, "%s\n", buffer);
		}
	*/
	fprintf(stdout, "\n");
	close(s);
	exit(0);
}
