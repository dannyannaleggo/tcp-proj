#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../module/MACheader.h"

const unsigned int BUFFSIZE = 1024;

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "gimme something to say as an argument!\n");
		exit(-1);
	}

	char buffer[BUFFSIZE];

	int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_MAC);
	if (-1 == s) {
		printf("error: socket did not open successfully\n");
		perror("?awegaweg");
		exit(-1);
	}

	/* set ourselves to nonzero privilege (should fail on server end) */
	int sec_code = 1;
	int rc = setsockopt(s, SOL_MAC, MAC_DOM, &sec_code, sizeof(sec_code));
	rc += setsockopt(s, SOL_MAC, MAC_LEV, &sec_code, sizeof(sec_code));
	rc += setsockopt(s, SOL_MAC, MAC_CAT, &sec_code, sizeof(sec_code));
	if (rc) {
		perror(":L");
		exit(-1);
	}

	struct sockaddr_in addr;
	memset((char *)&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr("10.67.7.169");
	addr.sin_port = htons(8080);
	socklen_t addrLen = sizeof(addr);

	// send the word to the server
	unsigned int echolen = strlen(argv[1]);
	printf("argv[1]: %s\n", argv[1]);
	printf("strlen(argv[1]): %i\n", echolen);

	/* the server should yell about permission denied here
         * cuz permission currently = 0*/
	if (sendto(s, argv[1], echolen, 0, (const struct sockaddr *)&addr,
		   addrLen) < 0) {
		perror("sendto failed");
		exit(-1);
	}

	/* set ourselves to privilege = 0 (should succeed on server end) */
	sec_code = 0;
	rc = setsockopt(s, SOL_MAC, MAC_DOM, &sec_code, sizeof(sec_code));
	rc += setsockopt(s, SOL_MAC, MAC_LEV, &sec_code, sizeof(sec_code));
	rc += setsockopt(s, SOL_MAC, MAC_CAT, &sec_code, sizeof(sec_code));

	if (rc) {
		perror(":L");
		exit(-1);
	}

	/* this should succeed on the server side now */
	if (sendto(s, argv[1], echolen, 0, (const struct sockaddr *)&addr,
		   addrLen) < 0) {
		perror("sendto failed");
		exit(-1);
	}

	fprintf(stdout, "\n");
	close(s);
	exit(0);
}
