#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

void handle_client(int clientfd);

int main()
{
	int socketfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (-1 == socketfd) {
		perror("??a");
		exit(-1);
	}

	struct sockaddr_in my_socket;
	memset(&my_socket, 0, sizeof(struct sockaddr_in));
	// initialize myself
	my_socket.sin_family = AF_INET;
	my_socket.sin_addr.s_addr = htonl(INADDR_ANY);
	my_socket.sin_port = htons(8080);

	// bind to port
	if (0 != bind(socketfd, (const struct sockaddr *)&my_socket,
		      sizeof(struct sockaddr_in))) {
		perror("?b");
		exit(-1);
	}

	handle_client(socketfd);

	if (0 != close(socketfd)) {
		perror("????");
		exit(-1);
	}
}

void handle_client(int socketfd)
{
	const size_t bufsize = 1024;
	char buffer[bufsize];
	int received_bytes = -1;
	/* int sent_bytes; */
	struct sockaddr_in client_addr;
	socklen_t size = sizeof(client_addr);
	while (true) {
		received_bytes =
		    recvfrom(socketfd, buffer, bufsize, 0,
			     (struct sockaddr *)&client_addr, &size);
		if (0 == received_bytes) {
			break;
		}
		if (-1 == received_bytes) {
			perror("?????");
			exit(-1);
		}
		buffer[received_bytes] = '\0';
		printf("%s\n", buffer);
		/* sent_bytes = sendto(socketfd, buffer, received_bytes, 0, */
		/* 		    (struct sockaddr *)&client_addr, size); */
		/* if (sent_bytes != received_bytes) { */
		/* 	perror("?????"); */
		/* 	exit(-1); */
		/* } */
	}
}
