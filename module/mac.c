#include <linux/kobject.h> //generic kernel object infrastructure

#include "mac_header.h"
#include "mac_impl.h"
#include "lops/Levels.h"
#include "f6os/Basic_Types.h"

#define VERBOSE=false

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anna Hwang <anna.s.hwang@vanderbilt.edu> and "
	      "Danny McClanahan <danieldmcclanahan@gmail.com>");
MODULE_DESCRIPTION("macooooo");

/* ==========================================================================*/
/* SYSFS INIT                                                                */
/* ==========================================================================*/

// Our wonderful awesome kobject
struct kobject *mac_kobj;
EXPORT_SYMBOL_GPL(mac_kobj);

/* Need to define functions used to modify/access attributes */
#define mac_ATTR_RO(_name) \
    static struct kobj_attribute _name##_attr = __ATTR_RO(_name)
#define KSM_ATTR(_name) \
    static struct kobj_attribute _name##_attr = \
        __ATTR(_name, 0644, _name##_show, _name##_store)

// Our struct of attributes for this module
static struct attribute *mac_attrs[] = {
    &labels_attr.attr,
    NULL, //not sure why this is here but it was in the reference i looked at
};

// Our attribute group
static struct attribute_group mac_attr_group = {
    .attrs = mac_attrs,
    .name = "mac",
};

/* ==========================================================================*/
/*                                                                           */
/* ==========================================================================*/



static int rc;

static int __init mac_init(void)
{
	if(VERBOSE)
        printk(KERN_INFO "Start mac_init()--------: %d", IPPROTO_mac);

	/* idk why the literal 1 is here but everyone else is doing it */
	rc = proto_register(&mac_prot, 1);
	if (rc) {
		proto_unregister(&mac_prot);
		printk(KERN_DEBUG "register protocol: FAILED");
		return rc;
	} else if (VERBOSE) {
        printk(KERN_INFO "register protocol: SUCCESS");
    }

	rc = inet_add_protocol(&mac_protocol, IPPROTO_mac);

	if (rc) {
		printk(KERN_DEBUG "inet add protocol: FAILED");
		return rc;
	} else if(VERBOSE) {
        printk(KERN_INFO "inet add protocol: SUCCESS");
    }

	inet_register_protosw(&mac_inetsw);

	if(VERBOSE)
        printk(KERN_INFO "inet register protosw: SUCCESS");

    mac_kobj = kobject_create_and_add("mac", kernel_obj);

	return 0;
}

static void __exit mac_cleanup(void)
{
	inet_unregister_protosw(&mac_inetsw);
	inet_del_protocol(&mac_protocol, IPPROTO_mac);
	proto_unregister(&mac_prot);
	printk(KERN_INFO "goodbye! u suk");
}

module_init(mac_init);
module_exit(mac_cleanup);
