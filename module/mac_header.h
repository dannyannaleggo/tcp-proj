#ifndef MACHEADER_H
#define MACHEADER_H

/* make sure these aren't in use in any active kernel patchset or module! */
#define IPPROTO_MAC 42
#define SOL_MAC IPPROTO_MAC

#define MAC_DOM 0
#define MAC_LEV 1
#define MAC_CAT 2

#endif  /* MACHEADER_H */
