#include "MACheader.h"
#include "MACimpl.h"

/* our protocol's functions */
int MAC_sock_init(struct sock *sk)
{
	struct MAC_sock *hl = (struct MAC_sock *)sk;
	hl->domain = 0;
	hl->level = 0;
	hl->category = 0;


	return 0;
}

int MAC_lib_setsockopt(struct sock *sk, int level, int optname,
		       char __user *optval, unsigned int optlen)
{
	struct MAC_sock *hl = (struct MAC_sock *)sk;
	int err = 0;
	int val;

	if (get_user(val, (int __user *)optval))
		return -EFAULT;

	switch (optname) {
	case MAC_DOM:
		/* TODO: only allow specific access levels */
		hl->domain = val;
		printk(KERN_DEBUG "domain: %d", val);
		break;

	case MAC_LEV:
		/* TODO: only allow specific access levels */
		hl->level = val;
		printk(KERN_DEBUG "level: %d", val);
		break;

	case MAC_CAT:
		/* TODO: only allow specific access levels */
		hl->category = val;
		printk(KERN_DEBUG "category: %d", val);
		break;

	default:
		err = -ENOPROTOOPT;
		break;
	}

	return err;
}

int MAC_setsockopt(struct sock *sk, int level, int optname, char __user *optval,
		   unsigned int optlen)
{
	if (level == SOL_MAC)
		return MAC_lib_setsockopt(sk, level, optname, optval, optlen);

	return udp_setsockopt(sk, level, optname, optval, optlen);
}

#ifdef CONFIG_COMPAT
int compat_MAC_setsockopt(struct sock *sk, int level, int optname,
			  char __user *optval, unsigned int optlen)
{
	if (level == SOL_MAC)
		return MAC_lib_setsockopt(sk, level, optname, optval, optlen);

	return compat_udp_setsockopt(sk, level, optname, optval, optlen);
}
#endif

int MAC_lib_getsockopt(struct sock *sk, int level, int optname,
		       char __user *optval, int __user *optlen)
{
	struct MAC_sock *hl = (struct MAC_sock *)sk;
	int val, len;

	if (get_user(len, optlen))
		return -EFAULT;

	/* TODO: figure out why this is required */
	len = min_t(unsigned int, len, sizeof(int));
	if (len < 0)
		return -EINVAL;

	if (get_user(val, (int __user *)optval))
		return -EFAULT;

	switch (optname) {
	case MAC_DOM:
		/* TODO: only allow specific access levels */
		val = hl->domain;
		printk(KERN_DEBUG "get dom: %d", val);
		break;

	case MAC_LEV:
		/* TODO: only allow specific access levels */
		val = hl->level;
		printk(KERN_DEBUG "get level: %d", val);
		break;

	case MAC_CAT:
		/* TODO: only allow specific access levels */
		val = hl->category;
                printk(KERN_DEBUG "get cat: %d", val);
		break;

	default:
		return -ENOPROTOOPT;
		break;
	}

	if (put_user(len, optlen))
		return -EFAULT;
	if (copy_to_user(optval, &val, len))
		return -EFAULT;
	return 0;
}

int MAC_getsockopt(struct sock *sk, int level, int optname, char __user *optval,
		   int __user *optlen)
{
	if (level == SOL_MAC)
		return MAC_lib_getsockopt(sk, level, optname, optval, optlen);

	return udp_getsockopt(sk, level, optname, optval, optlen);
}

#ifdef CONFIG_COMPAT
int compat_MAC_getsockopt(struct sock *sk, int level, int optname,
			  char __user *optval, int __user *optlen)
{
	if (level == SOL_MAC)
		return MAC_lib_getsockopt(sk, level, optname, optval, optlen);

	return compat_udp_getsockopt(sk, level, optname, optval, optlen);
}
#endif

int MAC_sendmsg(struct kiocb *iocb, struct sock *sk, struct msghdr *msg,
		size_t len)
{
	struct inet_sock *inet = inet_sk(sk);
	struct udp_sock *up = udp_sk(sk);
	struct flowi4 fl4_stack;
	struct flowi4 *fl4;
	int ulen = len;
	struct ipcm_cookie ipc;
	struct rtable *rt = NULL;
	int free = 0;
	int connected = 0;
	__be32 daddr, faddr, saddr;
	__be16 dport;
	u8 tos;
	int err, is_udplite = IS_UDPLITE(sk);
	int corkreq = up->corkflag || msg->msg_flags & MSG_MORE;
	int (*getfrag)(void *, char *, int, int, int, struct sk_buff *);
	struct sk_buff *skb;
	struct ip_options_data opt_copy;

	if (len > 0xFFFF)
		return -EMSGSIZE;

	/*
	 *	Check the flags.
	 */

	if (msg->msg_flags &
	    MSG_OOB) /* Mirror BSD error message compatibility */
		return -EOPNOTSUPP;

	ipc.opt = NULL;
	ipc.tx_flags = 0;
	ipc.ttl = 0;
	ipc.tos = -1;

	getfrag = is_udplite ? udplite_getfrag : ip_generic_getfrag;

	fl4 = &inet->cork.fl.u.ip4;
	if (up->pending) {
		/*
		 * There are pending frames.
		 * The socket lock must be held while it's corked.
		 */
		lock_sock(sk);
		if (likely(up->pending)) {
			if (unlikely(up->pending != AF_INET)) {
				release_sock(sk);
				return -EINVAL;
			}
			goto do_append_data;
		}
		release_sock(sk);
	}
	ulen += sizeof(struct MAChdr);

	/*
	 *	Get and verify the address.
	 */
	if (msg->msg_name) {
		DECLARE_SOCKADDR(struct sockaddr_in *, usin, msg->msg_name);
		if (msg->msg_namelen < sizeof(*usin))
			return -EINVAL;
		if (usin->sin_family != AF_INET) {
			if (usin->sin_family != AF_UNSPEC)
				return -EAFNOSUPPORT;
		}

		daddr = usin->sin_addr.s_addr;
		dport = usin->sin_port;
		if (dport == 0)
			return -EINVAL;
	} else {
		if (sk->sk_state != TCP_ESTABLISHED)
			return -EDESTADDRREQ;
		daddr = inet->inet_daddr;
		dport = inet->inet_dport;
		/* Open fast path for connected socket.
		   Route will not be used, if at least one option is set.
		 */
		connected = 1;
	}
	ipc.addr = inet->inet_saddr;

	ipc.oif = sk->sk_bound_dev_if;

	sock_tx_timestamp(sk, &ipc.tx_flags);

	if (msg->msg_controllen) {
		err = ip_cmsg_send(sock_net(sk), msg, &ipc,
				   sk->sk_family == AF_INET6);
		if (err)
			return err;
		if (ipc.opt)
			free = 1;
		connected = 0;
	}
	if (!ipc.opt) {
		struct ip_options_rcu *inet_opt;

		rcu_read_lock();
		inet_opt = rcu_dereference(inet->inet_opt);
		if (inet_opt) {
			memcpy(&opt_copy, inet_opt,
			       sizeof(*inet_opt) + inet_opt->opt.optlen);
			ipc.opt = &opt_copy.opt;
		}
		rcu_read_unlock();
	}

	saddr = ipc.addr;
	ipc.addr = faddr = daddr;

	if (ipc.opt && ipc.opt->opt.srr) {
		if (!daddr)
			return -EINVAL;
		faddr = ipc.opt->opt.faddr;
		connected = 0;
	}
	tos = get_rttos(&ipc, inet);
	if (sock_flag(sk, SOCK_LOCALROUTE) ||
	    (msg->msg_flags & MSG_DONTROUTE) ||
	    (ipc.opt && ipc.opt->opt.is_strictroute)) {
		tos |= RTO_ONLINK;
		connected = 0;
	}

	if (ipv4_is_multicast(daddr)) {
		if (!ipc.oif)
			ipc.oif = inet->mc_index;
		if (!saddr)
			saddr = inet->mc_addr;
		connected = 0;
	} else if (!ipc.oif)
		ipc.oif = inet->uc_index;

	if (connected)
		rt = (struct rtable *)sk_dst_check(sk, 0);

	if (rt == NULL) {
		struct net *net = sock_net(sk);

		fl4 = &fl4_stack;
		flowi4_init_output(fl4, ipc.oif, sk->sk_mark, tos,
				   RT_SCOPE_UNIVERSE, sk->sk_protocol,
				   inet_sk_flowi_flags(sk), faddr, saddr, dport,
				   inet->inet_sport);

		security_sk_classify_flow(sk, flowi4_to_flowi(fl4));
		rt = ip_route_output_flow(net, fl4, sk);
		if (IS_ERR(rt)) {
			err = PTR_ERR(rt);
			rt = NULL;
			if (err == -ENETUNREACH)
				IP_INC_STATS(net, IPSTATS_MIB_OUTNOROUTES);
			goto out;
		}

		err = -EACCES;
		if ((rt->rt_flags & RTCF_BROADCAST) &&
		    !sock_flag(sk, SOCK_BROADCAST))
			goto out;
		if (connected)
			sk_dst_set(sk, dst_clone(&rt->dst));
	}

	if (msg->msg_flags & MSG_CONFIRM)
		goto do_confirm;
back_from_confirm:

	saddr = fl4->saddr;
	if (!ipc.addr)
		daddr = ipc.addr = fl4->daddr;

	/* Lockless fast path for the non-corking case. */
	if (!corkreq) {
		skb = ip_make_skb(sk, fl4, getfrag, msg, ulen,
				  sizeof(struct MAChdr), &ipc, &rt,
				  msg->msg_flags);
		err = PTR_ERR(skb);
		if (!IS_ERR_OR_NULL(skb))
			/* MAC: changed to MAC_send_skb */
			err = MAC_send_skb(skb, fl4);
		goto out;
	}

	lock_sock(sk);
	if (unlikely(up->pending)) {
		/* The socket is already corked while preparing it. */
		/* ... which is an evident application bug. --ANK */
		release_sock(sk);

		net_dbg_ratelimited("cork app bug 2\n");
		err = -EINVAL;
		goto out;
	}
	/*
	 *	Now cork the socket to pend data.
	 */
	fl4 = &inet->cork.fl.u.ip4;
	fl4->daddr = daddr;
	fl4->saddr = saddr;
	fl4->fl4_dport = dport;
	fl4->fl4_sport = inet->inet_sport;
	up->pending = AF_INET;

do_append_data:
	up->len += ulen;
	err = ip_append_data(sk, fl4, getfrag, msg, ulen, sizeof(struct MAChdr),
			     &ipc, &rt, corkreq ? msg->msg_flags | MSG_MORE
						: msg->msg_flags);
	if (err)
		udp_flush_pending_frames(sk);
	else if (!corkreq)
		err = udp_push_pending_frames(sk);
	else if (unlikely(skb_queue_empty(&sk->sk_write_queue)))
		up->pending = 0;
	release_sock(sk);

out:
	ip_rt_put(rt);
	if (free)
		kfree(ipc.opt);
	if (!err)
		return len;
	/*
	 * ENOBUFS = no kernel mem, SOCK_NOSPACE = no sndbuf space.  Reporting
	 * ENOBUFS might not be good (it's not tunable per se), but otherwise
	 * we don't have a good statistic (IpOutDiscards but it can be too many
	 * things).  We could add another new stat but at least for now that
	 * seems like overkill.
	 */
	if (err == -ENOBUFS || test_bit(SOCK_NOSPACE, &sk->sk_socket->flags)) {
		UDP_INC_STATS_USER(sock_net(sk), UDP_MIB_SNDBUFERRORS,
				   is_udplite);
	}
	return err;

do_confirm:
	dst_confirm(&rt->dst);
	if (!(msg->msg_flags & MSG_PROBE) || len)
		goto back_from_confirm;
	err = 0;
	goto out;
}

int MAC_recvmsg(struct kiocb *iocb, struct sock *sk, struct msghdr *msg,
		size_t len, int noblock, int flags, int *addr_len)
{
	struct inet_sock *inet = inet_sk(sk);
	DECLARE_SOCKADDR(struct sockaddr_in *, sin, msg->msg_name);
	struct sk_buff *skb;
	unsigned int ulen, copied;
	int peeked, off = 0;
	int err;
	int is_udplite = IS_UDPLITE(sk);
	bool slow;
	/* MAC: add security members */
	struct MAC_sock *macsock;
	struct MAChdr *machdr;
	__u8 domain_sec;
	__u8 level_sec;
	__u8 category_sec;

	if (flags & MSG_ERRQUEUE)
		return ip_recv_error(sk, msg, len, addr_len);

try_again:
	skb = __skb_recv_datagram(sk, flags | (noblock ? MSG_DONTWAIT : 0),
				  &peeked, &off, &err);
	if (!skb)
		goto out;

	/* MAC: add security verification */
	macsock = MAC_SOCK(sk);
	domain_sec = macsock->domain;
	level_sec = macsock->level;
	category_sec = macsock->category;

	printk(KERN_DEBUG "domain_sec:   %d", domain_sec);
	printk(KERN_DEBUG "level_sec:    %d", level_sec);
	printk(KERN_DEBUG "category_sec: %d", category_sec);

	machdr = MAC_hdr(skb);

	/* TODO: change for the category stuff */
	if (domain_sec != machdr->domain || level_sec != machdr->level ||
	    category_sec != machdr->category) {
		err = -EACCES;
		goto out_free;
	}

	ulen = skb->len - sizeof(struct MAChdr);
	copied = len;
	if (copied > ulen)
		copied = ulen;
	else if (copied < ulen)
		msg->msg_flags |= MSG_TRUNC;

	/*
	 * If checksum is needed at all, try to do it while copying the
	 * data.  If the data is truncated, or if we only want a partial
	 * coverage checksum (UDP-Lite), do it before the copy.
	 */

	if (copied < ulen || UDP_SKB_CB(skb)->partial_cov) {
		if (udp_lib_checksum_complete(skb))
			goto csum_copy_err;
	}

	if (skb_csum_unnecessary(skb))
		err = skb_copy_datagram_msg(skb, sizeof(struct MAChdr), msg,
					    copied);
	else {
		err = skb_copy_and_csum_datagram_msg(skb, sizeof(struct MAChdr),
						     msg);

		if (err == -EINVAL)
			goto csum_copy_err;
	}

	if (unlikely(err)) {
		trace_kfree_skb(skb, udp_recvmsg);
		if (!peeked) {
			atomic_inc(&sk->sk_drops);
			UDP_INC_STATS_USER(sock_net(sk), UDP_MIB_INERRORS,
					   is_udplite);
		}
		goto out_free;
	}

	if (!peeked)
		UDP_INC_STATS_USER(sock_net(sk), UDP_MIB_INDATAGRAMS,
				   is_udplite);

	sock_recv_ts_and_drops(msg, sk, skb);

	/* Copy the address. */
	if (sin) {
		sin->sin_family = AF_INET;
		sin->sin_port = udp_hdr(skb)->source;
		sin->sin_addr.s_addr = ip_hdr(skb)->saddr;
		memset(sin->sin_zero, 0, sizeof(sin->sin_zero));
		*addr_len = sizeof(*sin);
	}
	if (inet->cmsg_flags)
		ip_cmsg_recv_offset(msg, skb, sizeof(struct MAChdr));

	err = copied;
	if (flags & MSG_TRUNC)
		err = ulen;

out_free:
	skb_free_datagram_locked(sk, skb);
out:
	return err;

csum_copy_err:
	slow = lock_sock_fast(sk);
	if (!skb_kill_datagram(sk, skb, flags)) {
		UDP_INC_STATS_USER(sock_net(sk), UDP_MIB_CSUMERRORS,
				   is_udplite);
		UDP_INC_STATS_USER(sock_net(sk), UDP_MIB_INERRORS, is_udplite);
	}
	unlock_sock_fast(sk, slow);

	if (noblock)
		return -EAGAIN;

	/* starting over for a new packet */
	msg->msg_flags &= ~MSG_TRUNC;
	goto try_again;
}

int MAC_send_skb(struct sk_buff *skb, struct flowi4 *fl4)
{
	struct sock *sk = skb->sk;
	struct inet_sock *inet = inet_sk(sk);
	struct udphdr *uh;
	struct MAChdr *mh;
	int err = 0;
	int is_udplite = IS_UDPLITE(sk);
	int offset = skb_transport_offset(skb);
	int len = skb->len - offset;
	__wsum csum = 0;

	/*
	 * Create a UDP header
	 */
	uh = udp_hdr(skb);
	uh->source = inet->inet_sport;
	uh->dest = fl4->fl4_dport;
	uh->len = htons(len);
	uh->check = 0;

	/* MAC: set security header in cb */
	/* FIXME: add all domain, level category stuff after writing/getting
	 * library */
	mh = MAC_hdr(skb);
	mh->domain = MAC_SOCK(sk)->domain;
	mh->level = MAC_SOCK(sk)->level;
	mh->category = MAC_SOCK(sk)->category;
	printk(KERN_DEBUG "sent domain:   %d", mh->domain);
	printk(KERN_DEBUG "sent level:    %d", mh->level);
	printk(KERN_DEBUG "sent category: %d", mh->category);

	if (is_udplite) /*     UDP-Lite      */
		csum = udplite_csum(skb);

	else if (sk->sk_no_check_tx) { /* UDP csum disabled */

		skb->ip_summed = CHECKSUM_NONE;
		goto send;

	} else if (skb->ip_summed == CHECKSUM_PARTIAL) { /* UDP hardware csum */

		udp4_hwcsum(skb, fl4->saddr, fl4->daddr);
		goto send;

	} else
		csum = udp_csum(skb);

	/* add protocol-dependent pseudo-header */
	uh->check = csum_tcpudp_magic(fl4->saddr, fl4->daddr, len,
				      sk->sk_protocol, csum);
	if (uh->check == 0)
		uh->check = CSUM_MANGLED_0;

send:
	err = ip_send_skb(sock_net(sk), skb);
	if (err) {
		if (err == -ENOBUFS && !inet->recverr) {
			UDP_INC_STATS_USER(sock_net(sk), UDP_MIB_SNDBUFERRORS,
					   is_udplite);
			err = 0;
		}
	} else
		UDP_INC_STATS_USER(sock_net(sk), UDP_MIB_OUTDATAGRAMS,
				   is_udplite);
	return err;
}

/* our protocol's structs */
struct proto MAC_prot = {
    .name = "MAC",
    .owner = THIS_MODULE,
    .close = udp_lib_close,
    .connect = ip4_datagram_connect,
    .disconnect = udp_disconnect,
    .ioctl = udp_ioctl,
    .destroy = udp_destroy_sock,
    .setsockopt = MAC_setsockopt,
    .getsockopt = MAC_getsockopt,
    .sendmsg = MAC_sendmsg,
    .init = MAC_sock_init,
    .recvmsg = MAC_recvmsg,
    .sendpage = udp_sendpage,
    .backlog_rcv = __udp_queue_rcv_skb,
    .release_cb = ip4_datagram_release_cb,
    .hash = udp_lib_hash,
    .unhash = udp_lib_unhash,
    .rehash = udp_v4_rehash,
    .get_port = udp_v4_get_port,
    .memory_allocated = &udp_memory_allocated,
    .sysctl_mem = sysctl_udp_mem,
    .sysctl_wmem = &sysctl_udp_wmem_min,
    .sysctl_rmem = &sysctl_udp_rmem_min,
    .obj_size = sizeof(struct MAC_sock),
    .slab_flags = SLAB_DESTROY_BY_RCU,
    .h.udp_table = &udp_table,
#ifdef CONFIG_COMPAT
    .compat_setsockopt = compat_MAC_setsockopt,
    .compat_getsockopt = compat_MAC_getsockopt,
#endif
    .clear_sk = sk_prot_clear_portaddr_nulls,
};

const struct net_protocol MAC_protocol = {
    .early_demux = udp_v4_early_demux,
    .handler = udp_rcv,
    .err_handler = udp_err,
    .no_policy = 1,
    .netns_ok = 1,
};

struct inet_protosw MAC_inetsw = {
    .type = SOCK_DGRAM,
    .protocol = IPPROTO_MAC,
    .prot = &MAC_prot,
    .ops = &inet_dgram_ops,
    .flags = INET_PROTOSW_ICSK, /* allow for removal of protocol */
};
