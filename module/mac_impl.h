#ifndef MACIMPL_H
#define MACIMPL_H

#include <linux/kernel.h>     /* required for module */
#include <linux/module.h>     /* required for module */
#include <net/protocol.h>     /* for proto_register */
#include <net/udp.h>	  /* for udp functions */
#include <net/udplite.h>      /* for udplite functions */
#include <net/protocol.h>     /* for protocol stuff */
#include <net/inet_common.h>  /* inet_dgram_ops */
#include <trace/events/skb.h> /* for trace_kfree_skb */

/*
 * DECLARATIONS FOR FUNCTIONS EXPORTED FROM THE KERNEL
 * TODO: remove all these, replace with our own functions, obviously. this
 * currently runs off a modified kernel which exports all these symbols. this
 * needs to be rectified ASAP.
 */

/* copied from udp_impl.h */
int __udp4_lib_rcv(struct sk_buff *, struct udp_table *, int);
void __udp4_lib_err(struct sk_buff *, u32, struct udp_table *);
int udp_v4_get_port(struct sock *sk, unsigned short snum);
int udp_setsockopt(struct sock *sk, int level, int optname, char __user *optval,
		   unsigned int optlen);
int udp_getsockopt(struct sock *sk, int level, int optname, char __user *optval,
		   int __user *optlen);
#ifdef CONFIG_COMPAT
int compat_udp_setsockopt(struct sock *sk, int level, int optname,
			  char __user *optval, unsigned int optlen);
int compat_udp_getsockopt(struct sock *sk, int level, int optname,
			  char __user *optval, int __user *optlen);
#endif
int udp_recvmsg(struct kiocb *iocb, struct sock *sk, struct msghdr *msg,
		size_t len, int noblock, int flags, int *addr_len);
int udp_sendpage(struct sock *sk, struct page *page, int offset, size_t size,
		 int flags);
int udp_queue_rcv_skb(struct sock *sk, struct sk_buff *skb);
void udp_destroy_sock(struct sock *sk);
#ifdef CONFIG_PROC_FS
int udp4_seq_show(struct seq_file *seq, void *v);
#endif
int udp_send_skb(struct sk_buff *skb, struct flowi4 *fl4);

/* static functions in udp.c */
void udp_v4_rehash(struct sock *sk);
int __udp_queue_rcv_skb(struct sock *sk, struct sk_buff *skb);

/* end declarations */

/* our own structs */
struct MAC_sock {
	struct udp_sock udp;

	// add the protocol implementation specific data members
	// per socket here from here on
	__u8 domain;
	__u8 level;
	__u8 category;


};

#define MAC_SOCK(__sk) ((struct MAC_sock *)(__sk))

struct MAChdr {
	struct udphdr uhdr;
	__u8 domain;
	__u8 level;
	__u8 category;
};

#define MAC_hdr(__skb) ((struct MAChdr *)skb_transport_header(__skb))

/* our own functions now */
int MAC_sock_init(struct sock *sk);

int MAC_lib_setsockopt(struct sock *sk, int level, int optname,
		       char __user *optval, unsigned int optlen);

int MAC_setsockopt(struct sock *sk, int level, int optname, char __user *optval,
		   unsigned int optlen);

#ifdef CONFIG_COMPAT
int conpat_MAC_setsockopt(struct sock *sk, int level, int optname,
			  char __user *optval, unsigned int optlen);
#endif

int MAC_lib_getsockopt(struct sock *sk, int level, int optname,
		       char __user *optval, int __user *optlen);

int MAC_getsockopt(struct sock *sk, int level, int optname, char __user *optval,
		   int __user *optlen);

#ifdef CONFIG_COMPAT
int compat_MAC_getsockopt(struct sock *sk, int level, int optname,
			  char __user *optval, int __user *optlen);
#endif

int MAC_sendmsg(struct kiocb *iocb, struct sock *sk, struct msghdr *msg,
		size_t len);

int MAC_recvmsg(struct kiocb *iocb, struct sock *sk, struct msghdr *msg,
		size_t len, int noblock, int flags, int *addr_len);

int MAC_send_skb(struct sk_buff *skb, struct flowi4 *fl4);

/* our protocol structs */
extern struct proto MAC_prot;

extern const struct net_protocol MAC_protocol;

extern struct inet_protosw MAC_inetsw;

#endif /* MACIMPL_H */
